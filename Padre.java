package mx.com.ids.beca.ejercicio2;

public class Padre {
	
	public void Publico() {
		System.out.println("Metodo publico del padre");
	}
	
	static void Estatico() {
		System.out.println("Metodo estatico del padre");
	}
	
	private void Privado() {
		System.out.println("Metodo privado del padre");
	}
	
	final void Constante() {
		System.out.println("Metodo final del padre");
	}
	
}
