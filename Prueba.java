package mx.com.ids.beca.ejercicio2;

/*Ejecuta los 4 métodos sobreescritos.
	¿Qué sucede?
 	R: El código no compila debido a que no se puede ejecutar los métodos privados y
 		el metodo constante muestra error desde la creación
 	*/

public class Prueba {

	public static void main(String[] args) {
		Hijo x = new Hijo();
		
		x.Publico();
		x.Estatico();
		x.Constante();
	}

}
