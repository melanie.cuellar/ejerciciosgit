package mx.com.ids.beca.ejercicio2;

/*Intenta sobrescribir los 4 métodos.
	a) ¿Qué sucede?
	
		R: Al sobre escribir el método final lo marco como error 
	
		a. ¿Compila?
		
		R: No.
*/

public class Hijo extends Padre {
	
	public void Publico() {
		System.out.println("Metodo publico del hijo");
	}
	
	static void Estatico() {
		System.out.println("Metodo estatico del hijo");
	}
	
	private void Privado() {
		System.out.println("Metodo privado del hijo");
	}
	
	final void Constante() {
		System.out.println("Metodo final del hijo");
	}
}
